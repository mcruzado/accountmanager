package com.technest.accountManager;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.technest.accountManager.model.Account;
import com.technest.accountManager.model.MoneyTransfer;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccountManagerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountControllerIntegrationTest {
     @Autowired
     private TestRestTemplate restTemplate;

     @LocalServerPort
     private int port;

     private String getRootUrl() {
         return "http://localhost:" + port + "/api";
     }

     @Test
     public void contextLoads() {

     }

     @Test
     public void testGetAccountsList() {
     HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/accounts",
        HttpMethod.GET, entity, String.class);  
        assertNotNull(response.getBody());
    }

    @Test
    public void testGetAccountById() {
        Account acc = restTemplate.getForObject(getRootUrl() + "/accounts/1", Account.class);
        assertNotNull(acc);
    }

    @Test
    public void testCreateAccount() {
        Account acc = new Account();
        acc.setName("acc-test-1");
        acc.setCurrency("EUR");
        acc.setTreasury(true);
        acc.setBalance(new BigDecimal(55.4));
        ResponseEntity<Account> response = restTemplate.postForEntity(getRootUrl() + "/accounts", acc, Account.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
    }

    @Test
    public void testUpdateAccountName() {
        final int id = 1;
        Account acc = restTemplate.getForObject(getRootUrl() + "/accounts/" + id, Account.class);
        acc.setName("acc-test-2");
        restTemplate.put(getRootUrl() + "/accounts/" + id, acc);
        Account updatedAcc = restTemplate.getForObject(getRootUrl() + "/accounts/" + id, Account.class);
        assertNotNull(updatedAcc);
    }
    
    @Test
    public void testTransferBetweenAccounts() throws JsonProcessingException, Exception {
        BigDecimal amount = new BigDecimal(100);
        MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
        ResponseEntity<Account> response = restTemplate.postForEntity(getRootUrl() + "/transfer", transfer, Account.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
    }
}