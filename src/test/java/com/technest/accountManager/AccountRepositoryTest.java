package com.technest.accountManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.technest.accountManager.model.Account;
import com.technest.accountManager.repo.AccountRepo;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {
	
	@Autowired
	private AccountRepo testAccountRepo;

	@Autowired
	private TestEntityManager testEntityManager;
	
	@Test
	public void saveAcc() {
		Account acc = new Account("test-default", "EUR", new BigDecimal(52), false);
		Long id = testEntityManager.persistAndGetId(acc, Long.class);
		assertNotNull(id);
		
		List<Account> targetAccounts = testAccountRepo.findMultiplesByName("test-default");
		Account target = targetAccounts.get(0);
		assertNotNull(target);
		assertEquals(target.getId(), target.getId());
		assertEquals(target.getBalance(), target.getBalance());
	}
	
	@Test
	public void getAcc() {
		Account acc = new Account("test-default", "USD", new BigDecimal(52.2), true);
		testAccountRepo.save(acc);
		
		Account accTarget = testAccountRepo.findOneByName("test-default");
		assertNotNull(accTarget);
		assertEquals(acc.getId(), accTarget.getId());
	}
	
	@Test
	public void getAccList() {
		Account acc = new Account("test-default", "USD", new BigDecimal(52.2), false);
		Account acc2 = new Account("test-default-2", "EUR", new BigDecimal(-52.2), true);
		
		testAccountRepo.save(acc);
		testAccountRepo.save(acc2);
		
		List<Account> targets = testAccountRepo.findAll();
		assertNotNull(targets);
		assert(targets.contains(acc));
		assert(targets.contains(acc2));
	}
}
