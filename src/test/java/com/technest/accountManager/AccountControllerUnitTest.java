package com.technest.accountManager;

import com.technest.accountManager.controller.AccountController;
import com.technest.accountManager.exc.PermissionDenied;
import com.technest.accountManager.exc.ResourceMissing;
import com.technest.accountManager.model.Account;
import com.technest.accountManager.model.MoneyTransfer;
import com.technest.accountManager.repo.AccountRepo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;


@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class AccountControllerUnitTest {
	
	@Mock
	AccountRepo accountRepo;
	
    @InjectMocks
	private AccountController testController;
    
    @Test
    public void testCreateAccountSuccessfully() throws PermissionDenied {
    	Account acc = new Account("test-account-one", "EUR", new BigDecimal(52.1), true);
    	
		Mockito.when(accountRepo.save(acc)).thenReturn(acc);
		
    	Account result = testController.createAccount(acc);
    	
    	Mockito.verify(accountRepo).save(acc);
    	
    	assertNotNull(result);
    	
    	assertEquals(result, acc);
    }
    
    @Test
    public void testCreateAccountWithUnsupportedCurrency() throws PermissionDenied {
    	Account acc = new Account("test-account-one", "GBP", new BigDecimal(52.1), true);
    	
    	PermissionDenied exc = assertThrows(PermissionDenied.class, () -> testController.createAccount(acc));
    	assertTrue(exc.getMessage().contains("Must introduce a valid currency"));
    }
    
	@Test
	public void testGetAllAccounts() {
		Account acc1 = new Account("test-account-one", "EUR", new BigDecimal(52.1), true);
		Account acc2 = new Account("test-account-two", "USD", new BigDecimal(12.4), false);
		
		ArrayList<Account> expected_list = new ArrayList<Account>();
		expected_list.add(acc1);
		expected_list.add(acc2);

		Mockito.when(accountRepo.findAll()).thenReturn(expected_list);
	  
		List<Account> result = testController.getAllAccounts();
	  
		Mockito.verify(accountRepo).findAll();
		
		assertNotNull(result);
		
		assertEquals(result, expected_list);
	}
	
	@Test
	public void testGetAccountSuccessfully() throws ResourceMissing {
		Account target = new Account("default", "EUR", new BigDecimal(4), false);
		
		Mockito.doReturn(Optional.of(target)).when(accountRepo).findById(Long.valueOf(1));
		
		ResponseEntity<Account> result = testController.getAccountById(Long.valueOf(1));
	
		assertNotNull(result);
		
		assertEquals(result, ResponseEntity.ok().body(target));
	}
	
	@Test
	public void testGetUnexistingAccount() throws ResourceMissing {
		
		ResourceMissing exc = assertThrows(ResourceMissing.class, () -> testController.getAccountById(Long.valueOf(1)));
		assertTrue(exc.getMessage().contains("Account missing"));
	}
	
	@Test
	public void updateAccountSuccessfully() throws ResourceMissing, PermissionDenied {
		Account original = new Account("default", "EUR", new BigDecimal(4), false);
		Account target = new Account("foo-bar", "EUR", new BigDecimal(4), false);

		Mockito.doReturn(Optional.of(original)).when(accountRepo).findById(Long.valueOf(1));
		Mockito.doReturn(target).when(accountRepo).save(original);
		
		ResponseEntity<Account> result = testController.updateAccount(Long.valueOf(1), target);	
		
		Mockito.verify(accountRepo).save(original);
		
		assertNotNull(result);
		
		assertEquals(result, ResponseEntity.ok(target));
	}
	
	@Test
	public void updateUnexistingAccount() throws ResourceMissing {
		Account target = new Account("foo-bar", "EUR", new BigDecimal(4), false);
		target.setId(2);
		
		ResourceMissing exc = assertThrows(ResourceMissing.class, () -> testController.updateAccount(Long.valueOf(1), target));
		assertTrue(exc.getMessage().contains("missing"));
	}
	
	@Test
	public void transferBetweenAccountsSuccessfully() throws ResourceMissing, PermissionDenied {
		
		Account original = new Account("default", "EUR", new BigDecimal(-4.2), true);
		original.setId(Long.valueOf(1));
		Account target = new Account("default2", "EUR", new BigDecimal(10), false);
		target.setId(Long.valueOf(2));
		
		BigDecimal amount = new BigDecimal(20.2);
		MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
		
		Mockito.doReturn(Optional.of(original)).when(accountRepo).findById(Long.valueOf(1));
		Mockito.doReturn(Optional.of(target)).when(accountRepo).findById(Long.valueOf(2));
		Mockito.doReturn(original).when(accountRepo).save(original);
		
		ResponseEntity<Account> result = testController.transferMoney(transfer);
		
		Mockito.verify(accountRepo).findById(Long.valueOf(1));
		Mockito.verify(accountRepo).findById(Long.valueOf(2));
		
		Mockito.verify(accountRepo).save(target);
		Mockito.verify(accountRepo).save(original);
		
		BigDecimal original_result_amount = result.getBody().getBalance();
		BigDecimal original_expected_amount = new BigDecimal(-4.2).subtract(amount);
		
		BigDecimal target_result_amount = target.getBalance();
		BigDecimal target_expected_amount = new BigDecimal(10).add(amount);
		
		assertNotNull(result);
		assertEquals(result.getBody(), original);
		
		assertEquals(original_result_amount, original_expected_amount);
		assertEquals(target_result_amount, target_expected_amount);
	}
	
	@Test
	public void transferBetweenAccountsFailedBecauseNotTreasuryAndNegativeBalance() throws PermissionDenied {
		
		Account original = new Account("default", "EUR", new BigDecimal(-4.2), false);
		original.setId(Long.valueOf(1));
		Account target = new Account("default2", "EUR", new BigDecimal(10), false);
		target.setId(Long.valueOf(2));
		
		BigDecimal amount = new BigDecimal(20.2);
		MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
		
		Mockito.doReturn(Optional.of(original)).when(accountRepo).findById(Long.valueOf(1));
		Mockito.doReturn(Optional.of(target)).when(accountRepo).findById(Long.valueOf(2));
		
		PermissionDenied exc = assertThrows(PermissionDenied.class, () -> testController.transferMoney(transfer));
		assertTrue(exc.getMessage().contains("Origin Account is not a treasury"));
	}
	
	@Test
	public void transferBetweenAccountsFailedBecauseNotEnoughFunds() throws PermissionDenied {
		
		Account original = new Account("default", "EUR", new BigDecimal(4.2), false);
		original.setId(Long.valueOf(1));
		Account target = new Account("default2", "EUR", new BigDecimal(10), false);
		target.setId(Long.valueOf(2));
		
		BigDecimal amount = new BigDecimal(20.2);
		MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
		
		Mockito.doReturn(Optional.of(original)).when(accountRepo).findById(Long.valueOf(1));
		Mockito.doReturn(Optional.of(target)).when(accountRepo).findById(Long.valueOf(2));
		
		PermissionDenied exc = assertThrows(PermissionDenied.class, () -> testController.transferMoney(transfer));
		assertTrue(exc.getMessage().contains("do not have enough money"));
	}
	
	@Test
	public void transferBetweenAccountsFailedBecauseNegativeAmountWasEntered() throws PermissionDenied {
		BigDecimal amount = new BigDecimal(-20.2);
		MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
		
		PermissionDenied exc = assertThrows(PermissionDenied.class, () -> testController.transferMoney(transfer));
		assertTrue(exc.getMessage().contains("negative or zero amount!"));
	}
	
	@Test
	public void transferBetweenAccountsFailedBecauseOriginAccountDoesntExist() throws ResourceMissing {
		Account target = new Account("default2", "EUR", new BigDecimal(10), false);
		target.setId(Long.valueOf(2));
		
		BigDecimal amount = new BigDecimal(20.2);
		MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
		
		ResourceMissing exc = assertThrows(ResourceMissing.class, () -> testController.transferMoney(transfer));
		assertTrue(exc.getMessage().contains("Origin Account doesnt exist"));
	}
	
	@Test
	public void transferBetweenAccountsFailedBecauseTargetAccountDoesntExist() throws ResourceMissing {
		Account origin = new Account("default1", "EUR", new BigDecimal(10), false);
		origin.setId(Long.valueOf(1));
		
		Mockito.doReturn(Optional.of(origin)).when(accountRepo).findById(Long.valueOf(1));
		
		BigDecimal amount = new BigDecimal(20.2);
		MoneyTransfer transfer = new MoneyTransfer(amount, Long.valueOf(1), Long.valueOf(2));
		
		ResourceMissing exc = assertThrows(ResourceMissing.class, () -> testController.transferMoney(transfer));
		assertTrue(exc.getMessage().contains("Destination Account"));
	}
}
