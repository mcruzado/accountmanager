package com.technest.accountManager.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.technest.accountManager.model.Account;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long>{
	List<Account> findMultiplesByName(String name);
	Account findOneByName(String name);
}