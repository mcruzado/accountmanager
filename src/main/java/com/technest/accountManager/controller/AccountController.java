package com.technest.accountManager.controller;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import com.technest.accountManager.model.Account;
import com.technest.accountManager.model.MoneyTransfer;
import com.technest.accountManager.repo.AccountRepo;

import lombok.RequiredArgsConstructor;

import com.technest.accountManager.exc.PermissionDenied;
import com.technest.accountManager.exc.ResourceMissing;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AccountController {
	
	static final List<String> VALID_CURRENCIES = Collections.unmodifiableList(
			new ArrayList<String>() {{
				add("USD");
				add("EUR");
			}});
	
	static final double EUR_USD_RATE = 1.17;
	static final double USD_EUR_RATE = 0.85;
	static final String DEFAULT_MSG = "Account missing with the id ";
	
	/* Method to check valid currencies */
	private Boolean validCurrency(String currency) {
		return VALID_CURRENCIES.contains(currency);
	}
	
	@Autowired
	private AccountRepo accountRepo; 

	
	/* MAPPINGS */
	
	@GetMapping("/accounts")
	public List<Account> getAllAccounts() {
		return accountRepo.findAll();
	}
	
	@GetMapping("/accounts/{id}")
	public ResponseEntity<Account> getAccountById(@PathVariable(value="id") Long accountId) 
	throws ResourceMissing {
		Account account = accountRepo.findById(accountId)
				.orElseThrow(() -> new ResourceMissing(DEFAULT_MSG + accountId));
		return ResponseEntity.ok().body(account);
	}
	
	@PostMapping("/accounts")
	public Account createAccount(@Valid @RequestBody Account account) throws PermissionDenied {
		if (!validCurrency(account.getCurrency())) {
			throw new PermissionDenied("Denied: Must introduce a valid currency");
		}
		return accountRepo.save(account);
	}
	
	/* Put method for update account name */
    @PutMapping("/accounts/{id}")
    public ResponseEntity <Account> updateAccount(@PathVariable(value = "id") Long accountId,
        @Valid @RequestBody Account accountDetails) throws ResourceMissing, PermissionDenied {
        Account account = accountRepo.findById(accountId)
            .orElseThrow(() -> new ResourceMissing(DEFAULT_MSG + accountId));
        
		account.setName(accountDetails.getName());
        
        final Account updatedAccount = accountRepo.save(account);
        return ResponseEntity.ok(updatedAccount);
    }
    
    /* Method to transfer money */
	@PostMapping("/transfer")
	public ResponseEntity<Account> transferMoney(@Valid @RequestBody MoneyTransfer moneyTransfer) 
	throws ResourceMissing, PermissionDenied {
		/* Check if Money transfer is positive */
		if (moneyTransfer.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
		    throw new PermissionDenied("Denied: You're trying to transfer a negative or zero amount!");
		}
		
		/* Finding origin account */
		Account originAccount = 
				accountRepo.findById(moneyTransfer.getOriginAccountId())
				.orElseThrow(() -> new ResourceMissing("Origin Account doesnt exist"));
		
		/* Finding target account */
		Account targetAccount =
				accountRepo.findById(moneyTransfer.getTargetAccountId())
				.orElseThrow(() -> new ResourceMissing("Destination Account doesnt exist"));
		
		/* Checking if Origin account got treasury property */
		final Boolean accountIsTreasury = originAccount.getTreasury();
		
		/* Getting balance from origin Account */
		final BigDecimal originAccountBalance = originAccount.getBalance();
		
		/* Checking if balance of origin account is greater than money we want to transfer */		
		Boolean balanceIsPositive = (originAccountBalance.compareTo(moneyTransfer.getAmount())) >= 0?true:false;
		
		if (!accountIsTreasury && !balanceIsPositive) {
			throw new PermissionDenied("Denied: Origin Account is not a treasury or you do not have enough money to transfer!");
		}
		
		/* Getting currencies from both accounts */
		final String originAccountCurrency = originAccount.getCurrency();
		final String targetAccountCurrency = targetAccount.getCurrency();
		
		/* Exchanging amount if necessary (Default currencies EUR or USD) */
		BigDecimal exchangedAmount;
		
		if (originAccountCurrency == targetAccountCurrency) {
			exchangedAmount = moneyTransfer.getAmount();
		} else {
			exchangedAmount = moneyTransfer.getAmount()
					.multiply(new BigDecimal((originAccountCurrency == "USD")?USD_EUR_RATE:EUR_USD_RATE));
		}
		
		/* Getting current balance from target account */
		final BigDecimal targetAccountBalance = targetAccount.getBalance();
		
		/* Subtracting money from Origin account. No exchanging because we are updating original account */
		originAccount.setBalance(originAccountBalance.subtract(moneyTransfer.getAmount()));
		
		/* Adding money to Target account */
		targetAccount.setBalance(targetAccountBalance.add(exchangedAmount));
		
		/* Updating accounts */
		final Account updatedOriginAccount = accountRepo.save(originAccount);
		accountRepo.save(targetAccount);
		
		/* Returning original account updated by default */
		return ResponseEntity.ok(updatedOriginAccount);
	}
}
