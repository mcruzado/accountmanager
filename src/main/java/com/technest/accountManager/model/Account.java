package com.technest.accountManager.model;

import javax.persistence.Table;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="accounts")
public class Account {
	
	private Long id;
	private String name;
	private String currency;
	private BigDecimal balance;
	private Boolean treasury;
	
	public Account() {
		
	}
	
	public Account(String name, String currency, BigDecimal balance, Boolean treasury) {
		this.name = name;
		this.currency = currency;
		this.balance = balance;
		this.treasury = treasury;
	}
	
	/** Id column (Auto-generated) */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
    public void setId(long id) {
        this.id = id;
    }
    
	/** Account name column declaration */
	@Column(name="name", nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/** Currency column declaration. By default, currency is EUR */
	@Column(name="currency", columnDefinition="VARCHAR(3) DEFAULT 'EUR'")
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/** Balance column declaration */
	@Column(name="balance", nullable=false)
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	/** Boolean column declaration. Only getter */
	@Column(name="treasury", columnDefinition="tinyint(1) DEFAULT 1")
	public boolean getTreasury() {
		return treasury;
	}
	public void setTreasury(Boolean treasury) {
		this.treasury = treasury;
	}
}