package com.technest.accountManager.model;

import java.math.BigDecimal;

public class MoneyTransfer {
	private BigDecimal amount;
	private Long originAccount;
	private Long targetAccount;
	
	public MoneyTransfer(BigDecimal amount, Long originAccount, Long targetAccount) {
		this.amount = amount;
		this.originAccount = originAccount;
		this.targetAccount = targetAccount;
	}
	
	public Long getOriginAccountId() {
		return originAccount;
	}
	
	public Long getTargetAccountId() {
		return targetAccount;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
}