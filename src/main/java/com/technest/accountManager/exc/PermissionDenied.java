package com.technest.accountManager.exc;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class PermissionDenied extends Exception {
	private static final long serialVersionUID = 1L;
	
	public PermissionDenied(String msg) {
		super(msg);
	}
}
