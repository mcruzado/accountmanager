package com.technest.accountManager.exc;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;

@ControllerAdvice
public class UniversalExceptionHandler {
    @ExceptionHandler(ResourceMissing.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceMissing ex, WebRequest request) {
         ErrorDetails details = new ErrorDetails(ex.getMessage(), request.getDescription(false), new Date());
         return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(ex.getMessage(), request.getDescription(false), new Date());
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}