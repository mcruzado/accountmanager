package com.technest.accountManager.exc;

import java.util.Date;

public class ErrorDetails {
	private String msg;
	private String details;
	private Date timestamp;
	
	public ErrorDetails(String msg, String details, Date timestamp) {
		super();
		this.msg = msg;
		this.details = details;
		this.timestamp = timestamp;
	}
	
	public String getMessage() {
		return msg;
	}
	
	public String getDetails() {
		return details;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
}
