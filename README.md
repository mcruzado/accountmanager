# AccountManager

Spring Boot Rest Exercise for technest. API Rest for manage transaction between accounts

## Declaration of the exercise:

We would like you to model an account service according to REST guidelines. It should be able to create, access, find accounts and to be able to transfer money between them.

An account seen by a REST client has the following details:
- Name: String
- Currency: Currency
- Balance: Money
- Treasury: Boolean

Requirements:

- Treasury property can be set only at creation time.
- Only treasury accounts (Treasury property) can have a negative balance.
- Non treasury accounts should block transfers if there is not enough balance.

What we are looking for:

- Solution modeled according to REST guidelines
- Easy to run and manually test (embedded DB, etc)
- All requirements are met
- Unit and component testing

How to do the exercise:

- Use a public repository
- Do not spend more than 4 hours from the first commit to the last
- Commit frequently to show the development process
- Add in the readme file all the decisions taken and any useful comment

## Tools & Technologies

- JDK 11
- Spring Framework 2.3.4.RELEASE
- Spring Boot 
- JPA/Hibernate (ORM)
- Maven (Package manager)
- H2 (Embedded Database)
- JUnit5

## Decissions

- I Decided to use H2 database in-memory store because we need an easy to test solution and we will have minimum data stored.
- I have used Hibernate to consolidate the relationship between the application and the database, in order to optimize the workflow

## Extra-Goals achieved
- Currency exchanger. If you transfer an amount between accounts with different currencies, it will be exchanged automatic

## Running

- You need to run the file 'AccountManagerApplication.java' located in 'src/main/java/com/technest/accountManager' as Spring Boot Application. I used JDK 11 and Spring 2.3.4.RELEASE

## Automated Testing

- Integration tests: src/test/java/com/technest/accountManager/AccountControllerIntegrationTest.java
- Unit tests (Account Controller): src/test/java/com/technest/accountManager/AccountControllerUnitTest.java
- Repository test: src/test/java/com/technest/accountManager/AccountRepositoryTest

## Manual Testing Endpoints

You can use Postman App to test the endpoints.

Feature: Create account
(POST - /api/accounts)

- You can create an account following this schema:

	Important: 
		- Please add Header 'Content-Type': 'application/json' in Postman before.
		- Only EUR and USD are allowed as currency

	```JSON
	{
		"name": "account1",
		"currency": "EUR",
		"treasury": true,
		"balance": 53.4
	}
	``

Feature: Modify account name
(PUT - /api/accounts/{id})

- You can modify an account name by id following this schema:

	```JSON
	{
		"name": "newName"
	}
	```

Feature: Access to an specific account data
(GET - /api/accounts/{id})

- You can access to an account info by id simply specifying a valid id in url

Feature: List all accounts data
(GET - /api/accounts)

- You can list all accounts info

Feature: Transfer money between accounts
(POST - /api/transfer)

- You can transfer money between accounts following this schema:

	*Important: 
		- Please add Header 'Content-Type': 'application/json' in Postman before.
		- An error will be displayed if the balance of the account is negative and the origin account is not treasury.
		- If the currencies are not equal, system will exchange the amount and will transfer it correctly.
		- Amount must be a positive number

	```JSON
	{
		"amount": 4828382.4,
		"originAccount": 1,
		"targetAccount": 2
	}
	```

## Repository

  - git clone https://gitlab.com/mcruzado/accountmanager.git
